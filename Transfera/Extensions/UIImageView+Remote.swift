//
//  UIImageView+Remote.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/22/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import AlamofireImage

private var transitionTime = 0.2

extension UIImageView {

    func setImage(withURL url: URL, completion: ((UIImage?) -> Void)? = nil) {
        af_setImage(
            withURL: url,
            placeholderImage: nil,
            filter: nil,
            progress: nil,
            progressQueue: DispatchQueue.main,
            imageTransition: .crossDissolve(transitionTime),
            runImageTransitionIfCached: false,
            completion: { completion?($0.result.value) })
    }

    func setImageFailable(withUrl url: URL?,
                          onCompletion: (() -> Void)? = nil) {
        guard let url = url else {
            setErrorImage()
            return
        }
        af_setImage(
            withURL: url,
            placeholderImage: nil,
            filter: nil,
            progress: nil,
            progressQueue: DispatchQueue.main,
            imageTransition: .crossDissolve(transitionTime),
            runImageTransitionIfCached: false,
            completion: {
                if $0.result.isFailure {
                    let error = $0.error as? AFIError
                    if error == nil || AFIError.requestCancelled != error {
                        self.setErrorImage()
                    }
                }
                onCompletion?()
        })
    }

    func setImageFailable(withUrlString urlString: String?) {
        guard let urlString = urlString else {
            return
        }
        guard let url = URL(string: urlString) else {
            setErrorImage()
            return
        }
        setImageFailable(withUrl: url)
    }

    func cancelRequest() {
        af_cancelImageRequest()
    }

    private func setErrorImage() {
        image = #imageLiteral(resourceName: "placeholder.png")
    }
}
