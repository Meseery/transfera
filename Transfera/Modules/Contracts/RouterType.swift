//
//  RouterType.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/19/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit
import RxSwift

protocol RouterType {
    func buildModule()
}

protocol NavigatableRouterType: RouterType {
    var navigationController: UINavigationController? {get set}
    func push(_ viewController: UIViewController)
    func pop()
}

extension NavigatableRouterType {
    func push(_ viewController: UIViewController) {
        DispatchQueue.main.async {
            self.navigationController?
                .pushViewController(viewController, animated: true)
        }
    }
    func pop() {
        navigationController?.popViewController(animated: true)
    }
}
