//
//  PresenterType.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/19/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import RxSwift

protocol ErrorPresenterType {
    var errorDescriptionText: Observable<String> { get }
}

protocol PresenterType: ErrorPresenterType {
    var screenNameObservable: Observable<String> {get}

    var didLoad: Observable<Error?> { get }
    var presentLoadingView: Observable<Bool> { get }
    var presentErrorView: Observable<Bool> { get }

    func reload()
    func requestData()
    func refreshData()
}
