//
//  AlbumPhotoCell.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/22/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit

class AlbumPhotoCell: UICollectionViewCell {
    @IBOutlet weak var albumPhotoNameLabel: UILabel!
    @IBOutlet weak var albumPhotoImage: UIImageView!

    static let cellId = String(describing: AlbumPhotoCell.self)

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    func configure(albumPhoto: AlbumPhoto) {
        albumPhotoNameLabel.text = albumPhoto.title
        albumPhotoImage.setImageFailable(withUrlString: albumPhoto.url)
        albumPhotoNameLabel.padding = .init(top: 0, left: 8, bottom: 0, right: 0)
        applyStyle()
    }

    private func applyStyle() {
        contentView.backgroundColor = UIColor.white
        contentView.layer.cornerRadius = 7.0
        contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 2.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 1.0
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: contentView.layer.cornerRadius).cgPath
    }
}
