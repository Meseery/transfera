//
//  AlbumPhotosViewController.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/22/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AlbumPhotosViewController: BaseViewController {
    // MARK: - Outlets
    @IBOutlet weak var albumPhotosCollectionView: UICollectionView! {
        didSet {
            albumPhotosCollectionView.register(
                UINib(nibName: AlbumPhotoCell.cellId, bundle: nil),
                forCellWithReuseIdentifier: AlbumPhotoCell.cellId)
        }
    }

    // MARK: - Private
    private let presenter: PresenterType

    // MARK: - Init
    override init(presenter: PresenterType) {
        self.presenter = presenter
        super.init(presenter: presenter)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRx()
        presenter.reload()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupCollectionView()
    }

    // MARK: - Setup
    func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        let padding: CGFloat = 20.0
        let side = albumPhotosCollectionView.frame.width/2 - padding
        layout.itemSize = CGSize(width: side, height: side)
        layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8)
        albumPhotosCollectionView.collectionViewLayout = layout
    }

    private func setupRx() {
        guard let presenter = presenter as? AlbumPhotosPresenterType
            else { return }
        let shouldShowEmptyMessage = Observable
            .combineLatest(presenter.albumPhotosObservable, presenter.presentLoadingView) { $0.count <= 0 && !$1 }
        shouldShowEmptyMessage.subscribe(onNext: { [weak self] show in
            guard let strongSelf = self else { return }
            if show {
                strongSelf
                    .albumPhotosCollectionView
                    .setEmptyMessage("Sorry, there's no Photos to be shown now")
            } else {
                strongSelf
                    .albumPhotosCollectionView
                    .restore()
            }
        }).disposed(by: bag)

        presenter.albumNameObservable.bind(to: self.navigationItem.rx.title).disposed(by: bag)

        presenter.albumPhotosObservable
            .bind(to: albumPhotosCollectionView.rx
                .items(cellIdentifier: AlbumPhotoCell.cellId,
                       cellType: UICollectionViewCell.self)) { _, item, cell in
                        guard let cell = cell as? AlbumPhotoCell else { return }

                        cell.configure(albumPhoto: item)
            }
            .disposed(by: bag)

        albumPhotosCollectionView.rx.itemSelected
            .throttle(1.0, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] indexPath in
                guard let strongSelf = self else { return }
                guard let presenter = strongSelf.presenter as? AlbumPhotosPresenterType else { return }
                presenter.didSelectCell(indexPath: indexPath)
            }).disposed(by: bag)

        albumPhotosCollectionView.rx.willDisplayCell
            .subscribe(onNext: { [weak self] cell, indexPath in
                guard let strongSelf = self else { return }
                let animation = AnimationFactory.makeFade(duration: 0.5, delayFactor: 0.05)
                let animator = Animator(animation: animation)
                animator.animate(cell: cell, at: indexPath, in: strongSelf.albumPhotosCollectionView)
            }).disposed(by: bag)
    }
}
