//
//  AlbumPhotosPresenter.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/22/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import RxSwift

protocol AlbumPhotosPresenterType: PresenterType {
    var albumPhotosObservable: Observable<[AlbumPhoto]> {get}
    var albumNameObservable: Observable<String> {get}
    func didSelectCell(indexPath: IndexPath)
}

class AlbumPhotosPresenter: BasePresenter, AlbumPhotosPresenterType {
    // MARK: - Input
    private var albumPhotosVariable: BehaviorSubject<[AlbumPhoto]> = BehaviorSubject(value: [])
    private var albumNameVariable: BehaviorSubject<String> = BehaviorSubject(value: "")

    // MARK: - Output
    lazy var albumPhotosObservable: Observable<[AlbumPhoto]> = self.albumPhotosVariable.asObservable()
    lazy var albumNameObservable: Observable<String> = self.albumNameVariable.asObservable()

    // MARK: - Private
    private let bag = DisposeBag()
    private let router: NavigatableRouterType
    private let dataManager: DataManager
    private let album: Album

    // MARK: - Init
    init(router: NavigatableRouterType,
         dataManager: DataManager,
         album: Album) {
        self.router = router
        self.dataManager = dataManager
        self.album = album
        super.init(router: router)

        self.albumNameVariable.onNext(album.title ?? "")
    }

    // MARK: - Setup
    override func requestData() {
        super.requestData()
        dataManager.fetchPhotos(forAlbum: album)
            .subscribe(onNext: { [weak self] photos in
                guard let strongSelf = self else { return }
                if let value = try? strongSelf.albumPhotosVariable.value() {
                    strongSelf.albumPhotosVariable.onNext(value + photos)
                } else {
                    strongSelf.albumPhotosVariable.onNext(photos)
                }
                strongSelf.setDidLoad.onNext(nil)
                },
                       onError: {[weak self] error in
                        guard let strongSelf = self else { return }
                        strongSelf.setDidLoad.onNext(error)
            })
            .disposed(by: self.bag)
    }

    func didSelectCell(indexPath: IndexPath) {
        guard let photoRouter = router as? AlbumPhotosRouter,
            let albumPhoto = try? albumPhotosVariable.value()[indexPath.row] else { return }

        guard let photoURL = URL(string: albumPhoto.url!) else {
            presentErrorViewVariable.onNext(true)
            return
        }
        photoRouter.navigateToPhoto(withURL: photoURL)
    }
}
