//
//  AlbumPhotosRouter.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/22/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit

class AlbumPhotosRouter: NavigatableRouterType {
    var navigationController: UINavigationController?
    var album: Album

    init(navigationController: UINavigationController?,
         album: Album) {
        self.album = album
        self.navigationController = navigationController
    }

    func buildModule() {
        let dataManager = DataManager.instance

        let presenter = AlbumPhotosPresenter(router: self,
                                             dataManager: dataManager,
                                             album: album)

        let viewController = AlbumPhotosViewController(presenter: presenter)

        self.push(viewController)
    }

    func navigateToPhoto(withURL url: URL) {
        AlbumPhotoRouter(navigationController: navigationController, photoURL: url).buildModule()
    }
}
