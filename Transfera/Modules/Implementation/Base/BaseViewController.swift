//
//  BaseViewController.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/19/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit
import RxSwift

class BaseViewController: UIViewController {
    fileprivate var presenter: PresenterType?
    let bag = DisposeBag()
    var loadingView: LoadingView?
    var errorView: AlertController?

    // MARK: - Init
    init() {
        super.init(nibName: nil, bundle: nil)
    }

    init(presenter: PresenterType) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRx()
    }

    // MARK: - Rx
    private func setupRx() {
        presenter?.presentLoadingView
            .subscribe(onNext: {[weak self] show in
                guard let strongSelf = self else { return }
                if show {
                    strongSelf.showLoading()
                } else {
                    strongSelf.hideLoading()
                }
            }).disposed(by: bag)

        presenter?.screenNameObservable
            .subscribe(onNext: { [weak self] screenName in
                guard let strongSelf = self else { return }
                strongSelf.title = screenName
            })
            .disposed(by: bag)

        presenter?.presentErrorView
            .subscribe(onNext: { [weak self] show in
                guard let strongSelf = self else { return }
                if show {
                    strongSelf.presentErrorView()
                } else {
                    strongSelf.hideErrorView()
                }

            }).disposed(by: bag)
    }

}

// MARK: - ViewController+Loading
extension BaseViewController: LoadingPresenter {
    func presentLoadingView() {
       showLoading()
    }

    func hideLoadingView() {
       hideLoading()
    }
}

// MARK: - ViewController+Error
extension BaseViewController: ErrorPresenter {
    func presentErrorView(with error: Error? = nil) {
        guard let error = error as? ErrorType else { return }
        showError(error, dismissAfter: 3)
    }

    func hideErrorView() {
        guard errorView != nil else { return }
        dismiss(animated: true, completion: nil)
    }
}
