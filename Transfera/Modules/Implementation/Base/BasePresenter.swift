//
//  BasePresenter.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/19/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import RxSwift

class BasePresenter: PresenterType {

    // MARK: - LyfeCycle properties
    var setDidLoad: PublishSubject<Error?> = PublishSubject()
    lazy var didLoad: Observable<Error?> = setDidLoad.asObservable()

    // MARK: - Present Views properties
    lazy var screenNameObservable: Observable<String> = self.screenNameVariable.asObservable()
    var screenNameVariable: BehaviorSubject<String> = BehaviorSubject(value: "")

    lazy var presentLoadingView: Observable<Bool> = self.presentLoadingViewVariable.asObservable()
    var presentLoadingViewVariable: BehaviorSubject<Bool> = BehaviorSubject(value: false)

    lazy var presentErrorView: Observable<Bool> = self.presentErrorViewVariable.asObservable()
    var presentErrorViewVariable: BehaviorSubject<Bool> = BehaviorSubject(value: false)

    var errorDescriptionViewVariable: BehaviorSubject<String> = BehaviorSubject(value: "")

    // MARK: - Private
    fileprivate let router: RouterType
    fileprivate let bag = DisposeBag()

    init(router: RouterType) {
        self.router = router
        setupCommonRx()
    }

    // MARK: - Request Data
    func reload() {
        presentLoadingViewVariable.onNext(true)
        requestData()
    }

    func refreshData() { requestData() }

    func requestData() {}

    func setupCommonRx() {
        didLoad
            .subscribe(onNext: { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf
                    .presentLoadingViewVariable
                    .onNext(false)
                if let error = error {
                    strongSelf
                        .manageError(error)
                } else {
                    strongSelf
                        .presentErrorViewVariable
                        .onNext(false)
                }
            })
            .disposed(by: bag)
    }

    // MARK: - Errors
    func manageError(_ error: Error) {
        presentErrorViewVariable.onNext(true)
        errorDescriptionViewVariable
            .onNext(error.localizedDescription)
    }

    var errorDescriptionText: Observable<String> {
        return errorDescriptionViewVariable.asObservable()
    }
}
