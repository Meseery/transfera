//
//  AlbumPhotoRouter.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/22/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit

class AlbumPhotoRouter: NavigatableRouterType {
    var navigationController: UINavigationController?
    var photoURL: URL

    init(navigationController: UINavigationController?,
         photoURL: URL) {
        self.photoURL = photoURL
        self.navigationController = navigationController
    }

    func buildModule() {
        let viewController = AlbumPhotoViewController(photoURL: photoURL)
        self.push(viewController)
    }
}
