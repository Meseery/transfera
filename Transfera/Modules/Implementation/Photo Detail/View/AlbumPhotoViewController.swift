//
//  AlbumPhotoViewController.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/22/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit

class AlbumPhotoViewController: BaseViewController {
    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.minimumZoomScale = 0.03
            scrollView.maximumZoomScale = 1.0
            scrollView.delegate = self
        }
    }

    // MARK: - Private
    private var photoURL: URL?

    private var photoView = UIImageView(frame: UIScreen.main.bounds) {
        didSet {
            photoView.sizeToFit()
            scrollView?.contentSize = photoView.frame.size
        }
    }

    // MARK: - Init
    init(photoURL: URL) {
        self.photoURL = photoURL
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.addSubview(photoView)
        fetchPhoto()
    }

    // MARK: - Photo fetching
    private func fetchPhoto() {
        guard let photoURL = photoURL else {
            presentErrorView()
            return
        }
        presentLoadingView()
        photoView.setImageFailable(withUrl: photoURL) {[weak self] in
            guard let strongSelf = self else { return }
            strongSelf.hideLoadingView()
        }
    }
}

// MARK: - Scrollview Delegate
extension AlbumPhotoViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return photoView
    }
}
