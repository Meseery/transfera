//
//  AlbumsRouter.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/22/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit
import Alamofire

class AlbumsRouter: NavigatableRouterType {
    var navigationController: UINavigationController?

    required init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }

    func buildModule() {
        let dataManager = DataManager.instance
        let presenter = AlbumsPresenter(router: self,
                                          dataManager: dataManager)
        let viewController = AlbumsViewController(presenter: presenter)
        push(viewController)
    }

    func navigateToPhotos(forAlbum album: Album) {
        AlbumPhotosRouter(navigationController: navigationController, album: album).buildModule()
    }
}
