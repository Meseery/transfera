//
//  AlbumsViewController.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/19/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AlbumsViewController: BaseViewController {
    // MARK: - Outlets
    @IBOutlet weak var albumsTableView: UITableView! {
        didSet {
            albumsTableView.register(
                UINib.init(nibName: AlbumCell.cellId, bundle: nil),
                                     forCellReuseIdentifier: AlbumCell.cellId)
            albumsTableView.rowHeight = AlbumCell.height
        }
    }
    // MARK: - Private
    private let presenter: PresenterType

    // MARK: - Init
    override init(presenter: PresenterType) {
        self.presenter = presenter
        super.init(presenter: presenter)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRx()
        presenter.reload()
    }

    // MARK: Setup
    private func setupRx() {
        guard let presenter = presenter as? AlbumsPresenterType
            else { return }
        let shouldShowEmptyMessage = Observable
            .combineLatest(presenter.albumsObservable, presenter.presentLoadingView) { $0.count <= 0 && !$1 }
        shouldShowEmptyMessage.subscribe(onNext: { [weak self] show in
            guard let strongSelf = self else { return }
            if show {
                strongSelf
                    .albumsTableView
                    .setEmptyMessage("Sorry, there's no albums to be shown now")

            } else {
                strongSelf
                    .albumsTableView
                    .restore()
            }
        }).disposed(by: bag)

        presenter.albumsObservable
            .bind(to: albumsTableView.rx.items) { tableView, row, element in
                let indexPath = IndexPath(item: row, section: 0)
                if let cell = tableView
                    .dequeueReusableCell(withIdentifier: AlbumCell.cellId,
                                         for: indexPath)
                    as? AlbumCell {
                    cell.configure(album: element)
                    return cell
                }

                return UITableViewCell()
            }.disposed(by: bag)

        albumsTableView.rx.itemSelected
            .throttle(1.0, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] indexPath in
                guard let strongSelf = self else { return }
                guard let presenter = strongSelf.presenter as? AlbumsPresenterType else { return }
                presenter.didSelectCell(indexPath: indexPath)
            }).disposed(by: bag)

        albumsTableView.rx.willDisplayCell
            .subscribe(onNext: { [weak self] cell, indexPath in
                guard let strongSelf = self else { return }
                let animation = AnimationFactory.makeSlideIn(duration: 0.5, delayFactor: 0.05)
                let animator = Animator(animation: animation)
                animator.animate(cell: cell, at: indexPath, in: strongSelf.albumsTableView)
            }).disposed(by: bag)
    }
}
