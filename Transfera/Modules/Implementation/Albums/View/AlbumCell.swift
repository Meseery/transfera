//
//  AlbumCell.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/19/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit

class AlbumCell: UITableViewCell {
    @IBOutlet weak var albumNameLabel: UILabel!

    static let height: CGFloat = 40.0
    static let cellId = String(describing: AlbumCell.self)

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    func configure(album: Album) {
        albumNameLabel.text = album.title
        albumNameLabel.padding = .init(top: 0, left: 18, bottom: 0, right: 10)
        applyStyle()
    }

    private func applyStyle() {
        self.selectionStyle = .none
        self.accessoryType = .disclosureIndicator
    }
}
