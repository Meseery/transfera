//
//  AlbumsPresenter.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/22/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import RxSwift

protocol AlbumsPresenterType: PresenterType {
    var albumsObservable: Observable<[Album]> {get}
    func didSelectCell(indexPath: IndexPath)
}

class AlbumsPresenter: BasePresenter, AlbumsPresenterType {

    // MARK: - Input
    private var albumsVariable: BehaviorSubject<[Album]> = BehaviorSubject(value: [])

    // MARK: - Output
    lazy var albumsObservable: Observable<[Album]> = albumsVariable.asObservable()

    // MARK: - Private
    private let bag = DisposeBag()
    private let router: NavigatableRouterType
    private let dataManager: DataManagerType

    // MARK: - Init
    init(router: NavigatableRouterType,
         dataManager: DataManagerType) {
        self.router = router
        self.dataManager = dataManager
        super.init(router: router)
        self.screenNameVariable.onNext("Albums")
    }

    // MARK: - Setup
    override func requestData() {
        dataManager.fetchAlbums()
            .subscribe(onNext: { [weak self] albums in
                guard let strongSelf = self else { return }
                if let oldValue = try? strongSelf.albumsVariable.value() {
                    strongSelf.albumsVariable.onNext(oldValue + albums)
                } else {
                    strongSelf.albumsVariable.onNext(albums)
                }
                    strongSelf.setDidLoad.onNext(nil)
                },
                    onError: {[weak self] error in
                        guard let strongSelf = self else { return }
                        strongSelf.setDidLoad.onNext(error)
            })
            .disposed(by: self.bag)
    }

    func didSelectCell(indexPath: IndexPath) {
        guard let router = router as? AlbumsRouter,
            let album = try? albumsVariable.value()[indexPath.row]
            else { return }
        router.navigateToPhotos(forAlbum: album)
    }
}
