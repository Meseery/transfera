//
//  Parser.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/23/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import RxSwift
import CoreData

class DataParser: DataParserType {
    let managedObjectContext: NSManagedObjectContext

    init(managedObjectContext: NSManagedObjectContext) {
        self.managedObjectContext = managedObjectContext
    }

    func parseData<T: Decodable>(_ data: Data,
                                 type: T.Type) throws -> T {
        let decoder = JSONDecoder()
        guard let contextUserInfoKey = CodingUserInfoKey.context
            else { fatalError("cannot find context key") }
        decoder.userInfo[contextUserInfoKey] = managedObjectContext

        guard let entity: T = try? decoder.decode(T.self, from: data) else {
            throw AppError.parsingError
        }
        return entity
    }
}

extension CodingUserInfoKey {
    static let context = CodingUserInfoKey(rawValue: "context")
}
