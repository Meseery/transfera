//
//  RemoteDataSource.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/16/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import RxSwift

class RemoteDataSource: DataSourceType {
    var networkService: NetworkServiceType
    var parseService: DataParserType
    var coreDataController: CoreDataController

    init(networkService: NetworkServiceType,
         coreDataController: CoreDataController) {
        self.networkService = networkService
        self.coreDataController = coreDataController
        self.parseService = DataParser(managedObjectContext: coreDataController.managedObjectContext)
    }

    func albums() -> Observable<[Album]> {
        let albumsEndPoint = AlbumEndPoint.albums
        return networkService
            .execute(albumsEndPoint)
            .map({ [weak self] data in
                guard let strongSelf = self else { throw AppError.parsingError }
                do {
                    return try strongSelf
                        .parseService
                        .parseData(data, type: [Album].self)
                } catch let error {
                    throw error
                }
        })
    }

    func photos(forAlbum album: Album) -> Observable<[AlbumPhoto]> {
        guard let albumId = album.albumId,
            let userId = album.userId
            else { return Observable.empty() }
        let photosEndPoint = AlbumEndPoint
            .photos(albumID: albumId, userID: userId)
        return networkService
            .execute(photosEndPoint)
            .map({ [weak self] data in
            guard let strongSelf = self else { throw AppError.parsingError }
                do {
                    return try strongSelf.parseService.parseData(data, type: [AlbumPhoto].self)
                } catch let error {
                    throw error
                }
        })
    }
}
