//
//  AlbumEndPoint.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/16/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Alamofire

enum AlbumEndPoint: EndPointType {
    case albums
    case photos(albumID: String, userID: String)

    var baseURL: String {
        return "https://jsonplaceholder.typicode.com/"
    }

    var path: URL? {
        switch self {
        case .albums:
            return try? baseURL
                .appending("albums")
                .asURL()
        case .photos:
            return try? baseURL
                .appending("photos")
                .asURL()
        }
    }

    var parameters: [String: Any]? {
        switch self {
        case .albums: return nil
        case let .photos(albumId, userId):
            return ["albumId": albumId,
                    "userId": userId]
        }
    }
    var method: HTTPMethod { return .get}
    var headers: [String: String]? { return nil }
}
