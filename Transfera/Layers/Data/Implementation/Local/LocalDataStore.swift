//
//  LocalDataStore.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/16/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import RxSwift
import CoreData

class LocalDataSource: DataStoreType {
    private let coreDataController: CoreDataController
    init(coreDataController: CoreDataController) {
        self.coreDataController = coreDataController
    }

    func saveAlbums(_ albums: [Album]) throws {
        let context = coreDataController.managedObjectContext
        try? context
            .batchDeleteEntities(ofType: Album.self)
        albums.forEach({ album in
            context.insert(album)
        })
        try? context.save()
    }

    func savePhotos(_ photos: [AlbumPhoto]) throws {
        let context = coreDataController.managedObjectContext
        try? context
            .batchDeleteEntities(ofType: AlbumPhoto.self)
        photos.forEach({ photo in
            context.insert(photo)
        })
        try context.save()
    }

    func albums() -> Observable<[Album]> {
        let managedObjectContext = coreDataController.persistentContainer.viewContext
        let fetchRequest = Album.fetchRequest
        do {
            let albums = try managedObjectContext.fetch(fetchRequest)
            return Observable.just(albums)
        } catch let error {
            return Observable.error(error)
        }
    }

    func photos(forAlbum album: Album) -> Observable<[AlbumPhoto]> {
        let managedObjectContext = coreDataController.managedObjectContext
        let fetchRequest = AlbumPhoto.fetchRequest
        let predicate = NSPredicate(format: "albumId == %@", album.albumId ?? "")
        fetchRequest.predicate = predicate
        do {
            let albumPhotos = try managedObjectContext.fetch(fetchRequest)
            return Observable.just(albumPhotos)
        } catch let error {
            return Observable.error(error)
        }
    }

    func clearStorage() {
        let managedObjectContext = coreDataController.managedObjectContext
        try? managedObjectContext
            .batchDeleteEntities(ofType: Album.self)
        try? managedObjectContext
            .batchDeleteEntities(ofType: AlbumPhoto.self)
    }
}

extension NSManagedObjectContext {
    func batchDeleteEntities<T: NSManagedObject>(ofType type: T.Type) throws {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: type.self))
        if #available(iOS 9.0, *) {
            let request = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            let result = try execute(request) as? NSBatchDeleteResult
            if let objectIDArray = result?.result as? [NSManagedObjectID] {
                let changes = [NSDeletedObjectsKey: objectIDArray]
                NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes, into: [self])
            }
        } else {
            fetchRequest.includesPropertyValues = false
            let results = try fetch(fetchRequest)
            if let actualResults = results as? [NSManagedObject], !actualResults.isEmpty {
                actualResults.forEach { delete($0) }
            }
        }
    }
}
