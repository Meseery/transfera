//
//  CoreDataStack.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/23/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import CoreData

class CoreDataController {
    let model: String

    public static var instance: CoreDataController {
        return CoreDataController(model: "Albums")
    }

    init(model: String) {
        self.model = model
    }

    private lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: model, withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: model,
                                            managedObjectModel: managedObjectModel)
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error {
                fatalError("Failed to load store: \(error)")
            }
            container
            .viewContext
            .automaticallyMergesChangesFromParent = true
            container
                .viewContext
                .mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        })
        return container
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        return persistentContainer.newBackgroundContext()
    }()
}
