//
//  DataManager.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/16/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import RxSwift
import Alamofire

class DataManager: DataManagerType {
    let remoteDataSource: DataSourceType
    let localDataSource: DataStoreType
    let coreDataController: CoreDataController

    public static var instance: DataManager {
        let coreDataController = CoreDataController.instance
        let networkService = NetworkService.instance
        let remoteDataSource = RemoteDataSource(
            networkService: networkService,
            coreDataController: coreDataController)
        let localDataSource = LocalDataSource(coreDataController: coreDataController)
        return DataManager(
            remoteDataSource: remoteDataSource,
            localDataSource: localDataSource,
            coreDataController: coreDataController)
    }

    private lazy var networkReachabilityManager = {
        return NetworkReachabilityManager()
    }()

    private lazy var isReachable: Bool = {
        if let isReachable = networkReachabilityManager?.isReachable,
            isReachable {
            return isReachable
        }
        return false
    }()

    init(remoteDataSource: RemoteDataSource,
         localDataSource: LocalDataSource,
         coreDataController: CoreDataController) {
        self.remoteDataSource = remoteDataSource
        self.localDataSource = localDataSource
        self.coreDataController = coreDataController
    }

    func fetchAlbums() -> Observable<[Album]> {
        if isReachable {
            return remoteDataSource
                .albums()
                .do(onNext: { [weak self] (albums) in
                guard let strongSelf = self else { return }
                try? strongSelf
                    .localDataSource
                    .saveAlbums(albums)
            })
        } else {
            return localDataSource.albums()
        }
    }

    func fetchPhotos(forAlbum album: Album) -> Observable<[AlbumPhoto]> {
        if isReachable {
            return remoteDataSource
                .photos(forAlbum: album)
                .do(onNext: { [weak self] (photos) in
                guard let strongSelf = self else { return }
                try? strongSelf
                    .localDataSource
                    .savePhotos(photos)
            })
        } else {
            return localDataSource.photos(forAlbum: album)
        }
    }
}
