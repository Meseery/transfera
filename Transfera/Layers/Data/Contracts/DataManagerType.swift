//
//  DataManagerType.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/25/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import RxSwift

protocol DataManagerType {
    func fetchAlbums() -> Observable<[Album]>
    func fetchPhotos(forAlbum album: Album) -> Observable<[AlbumPhoto]>
}
