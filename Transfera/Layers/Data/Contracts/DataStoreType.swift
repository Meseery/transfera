//
//  DataStore.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/23/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation

protocol DataStoreType: DataSourceType {
    func saveAlbums(_ albums: [Album]) throws
    func savePhotos(_ photos: [AlbumPhoto]) throws
}
