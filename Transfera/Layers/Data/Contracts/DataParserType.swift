//
//  DataParser.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/23/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation

protocol DataParserType {
    func parseData<T: Decodable>(_ data: Data,
                                 type: T.Type) throws -> T
}
