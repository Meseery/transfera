//
//  NetworkService.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/16/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Alamofire
import RxSwift

enum NetworkServiceConstants {
    static let timeout: TimeInterval = 10
}

class NetworkService: NetworkServiceType {
    private var sessionManager: SessionManager?

    public static var instance: NetworkService {
        return NetworkService(SessionManager.default)
    }

    init(_ sessionManager: SessionManager) {
        self.sessionManager = sessionManager
    }

    func execute(_ endPoint: EndPointType) -> Observable<Data> {
            return request(forEndpoint: endPoint)
    }

    private func request(forEndpoint endPoint: EndPointType) -> Observable<Data> {

        guard let isReachable = NetworkReachabilityManager()?.isReachable,
            isReachable else {
                return Observable.error(AppError.noInternetConection)
        }

        guard let manager = sessionManager else {
            return Observable.error(AppError.networkException)
        }

        guard let path = endPoint.path else {
            return Observable.error(AppError.invalidEndpoint)
        }

        return Observable.create { observer in
            let request = manager
                .request(path,
                         method: endPoint.method,
                         parameters: endPoint.parameters,
                         headers: endPoint.headers)
                .validate()
                .responseData(completionHandler: { response in
                    // MARK: - Request Success
                    if response.result.isSuccess,
                        let data = response.result.value {
                        observer.onNext(data)
                        return observer.onCompleted()
                    }
                    // MARK: - Error Handle
                    if response.result.isFailure,
                        let statusCode = response.response?.statusCode {
                        return observer
                            .onError(AppError
                                .networkError(code: statusCode))
                    }
                })

            return Disposables.create(with: {
                request.cancel()
            })
            }.timeout(
                NetworkServiceConstants.timeout,
                other: Observable.error(AppError.timeOutError),
                scheduler: MainScheduler.instance)
    }
}
