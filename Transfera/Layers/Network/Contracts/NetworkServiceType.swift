//
//  NetwrokService.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/16/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import RxSwift

protocol NetworkServiceType {
    func execute(_ endpoint: EndPointType) -> Observable<Data>
}
