//
//  ErrorType.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/16/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit.UIImage

protocol ErrorType: LocalizedError, CustomStringConvertible {
    var title: String { get }
    var image: UIImage? { get }
}

extension LocalizedError where Self: CustomStringConvertible {
    var errorDescription: String? {
        return description
    }
}
