//
//  NetworkError.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/16/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit.UIImage

enum AppError: ErrorType {
    case noInternetConection
    case networkException
    case invalidEndpoint
    case parsingError
    case timeOutError
    case networkError(code: Int)

    var title: String {
        return "OoO😲😲!"
    }

    var image: UIImage? { return nil }

    var description: String {
        switch self {
        case .noInternetConection:
            return "Please check your internet connection"
        case .invalidEndpoint, .networkException, .parsingError:
            return "Something went wrong, please try again shortly!"
        case .timeOutError:
            return "It takes more time than expected! Could you please try again later?!"
        case let .networkError(errorCode):
            switch errorCode {
            case 404:
                return "Sorry, couldn't find results matching your query"
            default:
                return "Sorry! unable to serve you this time"
            }
        }
    }
}
