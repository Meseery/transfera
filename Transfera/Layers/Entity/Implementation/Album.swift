//
//  Album.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/16/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import CoreData

class Album: NSManagedObject, EntityType {
    @NSManaged var albumId: String?
    @NSManaged var userId: String?
    @NSManaged var title: String?

    enum CodingKeys: String, CodingKey {
        case albumId = "id"
        case userId
        case title
    }

    required convenience init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context
            else { fatalError("cannot find context key") }

        guard let managedObjectContext = decoder
            .userInfo[contextUserInfoKey] as? NSManagedObjectContext
            else { fatalError("cannot Retrieve context") }

        guard let entity = NSEntityDescription
            .entity(forEntityName: "Album",
                    in: managedObjectContext)
            else { fatalError("cannot init Album managed object") }

        self.init(entity: entity, insertInto: managedObjectContext)

        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let userId = try? container.decode(Int.self, forKey: .userId) {
            self.userId = String(userId)
        }
        if let albumId = try? container.decode(Int.self, forKey: .albumId) {
            self.albumId = String(albumId)
        }
        title = try? container.decode(String.self, forKey: .title)
    }
}

extension Album {
    static var fetchRequest: NSFetchRequest<Album> {
        return NSFetchRequest(entityName: "Album")
    }
}
