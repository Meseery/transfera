//
//  AlbumPhoto.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/16/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import CoreData

class AlbumPhoto: NSManagedObject, EntityType {
    @NSManaged var photoId: String?
    @NSManaged var albumId: String?
    @NSManaged var title: String?
    @NSManaged var url: String?
    @NSManaged var thumbnailUrl: String?

    enum CodingKeys: String, CodingKey {
        case photoId = "id"
        case albumId = "albumId"
        case title
        case url
        case thumbnailUrl
    }

    required convenience init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context
            else { fatalError("cannot find context key") }

        guard let managedObjectContext = decoder
            .userInfo[contextUserInfoKey] as? NSManagedObjectContext
            else { fatalError("cannot Retrieve context") }

        guard let entity = NSEntityDescription
            .entity(forEntityName: "AlbumPhoto",
                    in: managedObjectContext)
            else { fatalError("cannot init AlbumPhoto managed object") }

        self.init(entity: entity, insertInto: managedObjectContext)

        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let photoId = try? container.decode(Int.self, forKey: .photoId) {
            self.photoId = String(photoId)
        }
        if let albumId = try? container.decode(Int.self, forKey: .albumId) {
            self.albumId = String(albumId)
        }
        title = try? container.decode(String.self, forKey: .title)
        url = try? container.decode(String.self, forKey: .url)
        thumbnailUrl = try? container.decode(String.self, forKey: .thumbnailUrl)
    }
}

extension AlbumPhoto {
    static var fetchRequest: NSFetchRequest<AlbumPhoto> {
        return NSFetchRequest(entityName: "AlbumPhoto")
    }
}
