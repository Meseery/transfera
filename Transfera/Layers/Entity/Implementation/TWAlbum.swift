//
//  TWAlbum.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/16/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation

struct Album: Codable {
    var albumId: Int
    var userId: Int
    var title: String

    enum CodingKeys: String, CodingKey {
        case albumId = "id"
        case userId
        case title
    }
}
