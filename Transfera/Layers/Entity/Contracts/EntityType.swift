//
//  Entity.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/16/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import CoreData

protocol EntityType: Decodable where Self: NSManagedObject {}
