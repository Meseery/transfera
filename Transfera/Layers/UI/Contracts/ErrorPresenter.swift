//
//  ErrorDisplayable.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/17/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit.UIViewController

protocol ErrorPresenter {
    func showError(_ error: ErrorType,
                   dismissAfter seconds: Int?)
}

extension ErrorPresenter where Self: UIViewController {
    func showError(_ error: ErrorType,
                   dismissAfter seconds: Int? = nil) {
        let title = error.title
        let description = error.localizedDescription
        let alertViewController = AlertController(title: title,
                                                  message: description,
                                                  preferredStyle: .alert)
        if let image = error.image {
            alertViewController.setTitleImage(image)
        }
        present(alertViewController, animated: true)

        if let seconds = seconds {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(seconds)) {
                alertViewController.dismiss(animated: true)
            }
        }
    }
}
