//
//  LoadingPresenter.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/17/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit.UIViewController

protocol LoadingPresenter {
    func showLoading(_ message: String?)
    func hideLoading()
}

extension LoadingPresenter where Self: UIViewController {
    func showLoading(_ message: String? = "Loading...") {
        let loadingView = LoadingView(frame: self.view.bounds,
                                      color: UIColor.orange,
                                      message: message)
        loadingView.present()
    }

    func hideLoading() {
        LoadingView.dismiss()
    }
}
