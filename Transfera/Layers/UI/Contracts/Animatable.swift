//
//  Animatable.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/17/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit.UITableViewCell
import UIKit.UICollectionViewCell

protocol AnimatableCell where Self: UIView {}
protocol AnimatableListView {
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool
}

extension UITableViewCell: AnimatableCell {}
extension UICollectionViewCell: AnimatableCell {}
