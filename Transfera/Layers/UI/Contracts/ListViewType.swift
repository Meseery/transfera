//
//  UITableView+Empty.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/25/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit

protocol ListViewType: EmptyMessageViewType where Self: UIView {
    var backgroundView: UIView? { get set }
}
