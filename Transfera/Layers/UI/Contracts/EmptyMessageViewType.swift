//
//  EmptyMessageViewType.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/25/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation

protocol EmptyMessageViewType {
    mutating func setEmptyMessage(_ message: String)
    mutating func restore()
}
