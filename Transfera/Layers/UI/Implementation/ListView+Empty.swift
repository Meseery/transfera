//
//  ListView+Empty.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/25/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit

extension UITableView: ListViewType {}
extension UICollectionView: ListViewType {}

extension ListViewType {
    mutating func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0,
                                                 y: 0,
                                                 width: self.bounds.size.width,
                                                 height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 16)
        messageLabel.sizeToFit()

        backgroundView = messageLabel
    }

    mutating func restore() {
        backgroundView = nil
    }
}
