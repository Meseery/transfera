//
//  Animator.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/17/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit

final class Animator {
    private var hasAnimatedAllCells = false
    private let animation: Animation

    init(animation: @escaping Animation) {
        self.animation = animation
    }

    func animate(cell: AnimatableCell,
                 at indexPath: IndexPath,
                 in listView: AnimatableListView) {
        guard !hasAnimatedAllCells else {
            return
        }

        animation(cell, indexPath, listView)

        hasAnimatedAllCells = listView.isLastVisibleCell(at: indexPath)
    }
}

extension UITableView: AnimatableListView {
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = indexPathsForVisibleRows?.last else {
            return false
        }

        return lastIndexPath == indexPath
    }
}

extension UICollectionView: AnimatableListView {
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = indexPathsForVisibleItems.last else {
            return false
        }

        return lastIndexPath == indexPath
    }
}
