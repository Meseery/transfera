//
//  AppCoordinator.swift
//  Transfera
//
//  Created by Mohamed EL Meseery on 3/16/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit

protocol AppCoordinatorType {
    static func start(on window: UIWindow?)
}

final public class AppCoordinator: AppCoordinatorType {
    static public func start(on window: UIWindow?) {
        let navigationController = UINavigationController()
        let albumsRouter = AlbumsRouter(navigationController: navigationController)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        albumsRouter.buildModule()
    }
}
