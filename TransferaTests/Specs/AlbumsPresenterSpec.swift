import Quick
import Nimble
import RxSwift
@testable import Transfera

class AlbumsPresenterSpec: QuickSpec {
    override func spec() {
        let dataManagerMock = DataManagerMock()

        let routerMock = AlbumsRouterMock(navigationController: UINavigationController())

        let bag = DisposeBag()

        var sut: AlbumsPresenterType!

        beforeEach {

            sut = AlbumsPresenter(router: routerMock,
                                    dataManager: dataManagerMock)

            waitUntil(timeout: 10) { done in
                sut.didLoad
                    .subscribe(onNext: { _ in
                        done()
                    })
                    .disposed(by: bag)
                sut.reload()
            }
        }

        describe("Provide data") {
            it("albumsObservable should provide array of Albums on success") {

                var response = false

                sut.albumsObservable.subscribe(onNext: { _ in

                    response = true

                }).disposed(by: bag)

                expect(response).toEventually(equal(true))
            }
        }
    }
}
