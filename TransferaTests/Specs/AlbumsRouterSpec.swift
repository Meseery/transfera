import Quick
import Nimble
@testable import Transfera

class AlbumsRouterSpec: QuickSpec {
    override func spec() {
        var navigationController: UINavigationController!
        var sut: NavigatableRouterType!

        beforeEach {
            navigationController = UINavigationController()
            sut = AlbumsRouterMock(navigationController: navigationController)

            UIApplication.shared.keyWindow!.rootViewController = navigationController
            _ = navigationController.view
        }

        describe("Building") {
            it("should build with a AlbumsViewController") {
                sut.buildModule()
                expect(navigationController.topViewController)
                    .toEventually(beAnInstanceOf(AlbumsViewController.self))
            }
        }

        describe("Navigation") {
            it("should push a PhotosViewController when navigateToPhotos is called") {
                guard let sut = sut as? AlbumsRouterMock else {
                    fail("Album")
                    return
                }
                let dataManagerMock = DataManagerMock()
                guard let album = dataManagerMock.albumMock else { return }
                sut.navigateToPhotos(forAlbum: album)
                expect(navigationController.topViewController)
                    .toEventually(beAnInstanceOf(AlbumPhotosViewController.self))
            }
        }
    }
}
