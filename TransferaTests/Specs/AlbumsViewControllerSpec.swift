import Quick
import Nimble
@testable import Transfera

class AlbumsViewControllerSpec: QuickSpec {

    override func spec() {
        var sut: AlbumsViewController!

        beforeEach {
            let presenter = AlbumsPresenterMock(dataManager: DataManagerMock())
            sut = AlbumsViewController(presenter: presenter)
            _ = sut.view
        }

        describe("Creation") {
            it("should show title") {
                expect(sut.title).to(match("Album"))
            }

            it("should show 4 albums") {
                expect(sut.albumsTableView.numberOfRows(inSection: 0)).to(be(4))
            }
        }

    }
}
