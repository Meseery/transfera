import Quick
import Nimble
@testable import Transfera

class AlbumSpec: QuickSpec {
    override func spec() {
        let coreDataHelper = CoreDataHelper()
        context("UT: Testing Album Model with core data controller") {
            describe("On fetching json data from remote/local service,") {
                it("it should be mapped to a valid Album Model") {
                    let jsonData = coreDataHelper.jsonData(for: "Albums")
                    do {
                        let managedObjectContext = coreDataHelper.mockPersistentContainer.viewContext
                        let decoder = JSONDecoder()
                        decoder.userInfo[CodingUserInfoKey.context!] = managedObjectContext
                        let sut = try decoder.decode([Album].self, from: jsonData!)
                        expect(sut).toNot(beNil())
                        expect(sut.count).to(equal(1))
                        expect(sut.first?.albumId).to(equal("1"))
                        expect(sut.first?.userId).to(equal("1"))
                        expect(sut.first?.title).to(match("quidem molestiae enim"))
                    } catch let error {
                        fatalError(error.localizedDescription)
                    }
                }
            }
        }
    }
}
