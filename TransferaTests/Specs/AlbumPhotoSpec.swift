import Quick
import Nimble
@testable import Transfera

class AlbumPhotoSpec: QuickSpec {
    override func spec() {
        let coreDataHelper = CoreDataHelper()
        context("UT: Testing AlbumPhoto Model with core data controller") {
            describe("On fetching json data from remote/local service,") {
                it("it should be mapped to a valid AlbumPhoto Model") {
                    let jsonData = coreDataHelper.jsonData(for: "AlbumPhotos")
                    do {
                        let managedObjectContext = coreDataHelper.mockPersistentContainer.viewContext
                        let decoder = JSONDecoder()
                        decoder.userInfo[CodingUserInfoKey.context!] = managedObjectContext
                        let sut = try decoder.decode([AlbumPhoto].self, from: jsonData!)
                        expect(sut).toNot(beNil())
                        expect(sut.count).to(equal(5))
                        expect(sut.first?.albumId).to(equal("1"))
                    } catch let error {
                        fatalError(error.localizedDescription)
                    }
                }
            }
        }
    }
}
