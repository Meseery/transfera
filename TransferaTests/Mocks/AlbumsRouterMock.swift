//
//  AlbumsRouterMock.swift
//  TransferaTests
//
//  Created by Mohamed EL Meseery on 3/25/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit
@testable import Transfera

class AlbumsRouterMock: NavigatableRouterType {
    var navigationController: UINavigationController?
    var navigateToPhotosCalledInRouter = false

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func buildModule() {
        let dataManager = DataManagerMock.init()
        let presenter = AlbumsPresenter(router: self,
                                        dataManager: dataManager)
        let viewController = AlbumsViewController(presenter: presenter)
        push(viewController)
    }

    func navigateToPhotos(forAlbum album: Album) {
        AlbumPhotosRouter(navigationController: navigationController,
                          album: album)
            .buildModule()
    }
}
