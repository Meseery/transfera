//
//  CoreDataControllerMock.swift
//  TransferaTests
//
//  Created by Mohamed EL Meseery on 3/25/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import CoreData
import Quick
import Nimble

class CoreDataHelper {
    lazy var managedObjectModel: NSManagedObjectModel = {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle.main])!
        return managedObjectModel
    }()

    lazy var mockPersistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "AlbumsMock",
                                              managedObjectModel: managedObjectModel)
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false

        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            precondition(description.type == NSInMemoryStoreType)
            if let error = error {
                XCTFail("Error creating the in-memory NSPersistentContainer mock: \(error)")
            }
        }
        return container
    }()

    func jsonData(for fileName: String) -> Data? {
        let bundle = Bundle(for: type(of: self))
        guard let path = bundle.path(forResource: fileName,
                                     ofType: "json") else { return nil }
        let data = try? Data(contentsOf: URL(fileURLWithPath: path))
        return data
    }

    func numberOfItemsInPersistentStore(for entityName: String) -> Int {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let results = try? mockPersistentContainer.viewContext.fetch(request)
        return results?.count ?? 0
    }

    func clearStorage(for entityName: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let context = mockPersistentContainer.viewContext
        if let objs = try? context.fetch(fetchRequest) {
            for case let obj as NSManagedObject in objs {
                context.delete(obj)
            }
            try? context.save()
        }
    }
}
