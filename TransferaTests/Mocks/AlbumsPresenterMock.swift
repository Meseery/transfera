//
//  AlbumsPresenterMock.swift
//  TransferaTests
//
//  Created by Mohamed EL Meseery on 3/25/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit
import RxSwift
@testable import Transfera

class AlbumsPresenterMock: AlbumsPresenterType {
    var dataManager: DataManagerType

    var albumsObservable: Observable<[Album]> = Observable.of([])

    var screenNameObservable: Observable<String> = Observable.of("Albums")

    var didLoad: Observable<Error?> = Observable.of(nil)

    var presentLoadingView: Observable<Bool> = Observable.of(false)

    var presentErrorView: Observable<Bool> = Observable.of(false)

    var descriptionText: Observable<String> = Observable.of("")

    var errorDescriptionText: Observable<String> = Observable.of("")

    init(dataManager: DataManagerType) {
        self.dataManager = dataManager
        albumsObservable = dataManager.fetchAlbums()
    }

    func didSelectCell(indexPath: IndexPath) {}

    func reload() {}

    func requestData() {}

    func refreshData() {}
}
