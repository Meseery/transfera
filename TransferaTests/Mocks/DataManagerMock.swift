//
//  DataManagerMock.swift
//  TransferaTests
//
//  Created by Mohamed EL Meseery on 3/25/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import RxSwift
import CoreData
@testable import Transfera

class DataManagerMock: DataManagerType {
    let coreDataHelper = CoreDataHelper()
    var albumMock: Album? {
        let album = NSEntityDescription.insertNewObject(forEntityName: "Album",
                                                        into: coreDataHelper
                                                            .mockPersistentContainer
                                                            .viewContext)
        album.setValue("1", forKey: "albumId")
        album.setValue("1", forKey: "userId")
        album.setValue("Mock Album", forKey: "title")
        return album as? Album
    }

    var albumPhotoMock: AlbumPhoto? {
        let album = NSEntityDescription.insertNewObject(forEntityName: "AlbumPhoto",
                                                        into: coreDataHelper
                                                            .mockPersistentContainer
                                                            .viewContext)
        album.setValue("1", forKey: "photoId")
        album.setValue("1", forKey: "albumId")
        album.setValue("Mock AlbumPhoto", forKey: "title")
        album.setValue("photo URL", forKey: "url")
        album.setValue("photoThumbURL", forKey: "thumbnailUrl")
        return album as? AlbumPhoto
    }

    func fetchAlbums() -> Observable<[Album]> {
        guard let album = albumMock else {
            return Observable.of([])
        }
        return Observable.of([album, album, album, album])
    }

    func fetchPhotos(forAlbum album: Album) -> Observable<[AlbumPhoto]> {
        guard let albumPhoto = albumPhotoMock else {
            return Observable.of([])
        }
        return Observable.of([albumPhoto])
    }
}
